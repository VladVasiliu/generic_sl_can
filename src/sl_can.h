#ifndef _SL_CAN_H_
#define _SL_CAN_H_
#define SL_CAN_REMOTE_FRAME (0x80000000ul)
typedef struct
{
    uint8 (*isTxReady)(void);
    void  (*Write)(uint8 *data,uint16 size);
} slcan_SerialIfc;

typedef struct
{
   uint8 (*CanStart)(void);
   void  (*CanStop)(void);
   uint8 (*SetBaudrate)(uint32 Baud);
   uint8 (*SetBtr)(uint8 BTR0,uint8 BTR1);
   uint8 (*CanTx)(uint32 ID, uint8 dlc, uint8 *data);
   uint8 (*CanSetCodeRegister)(uint32 code);
   uint8 (*CanSetMaskCodeRegister)(uint32 mask);
   uint8 (*CanReadStatusFlags)(void);
}slcan_CanIfc;

typedef enum
{
    slcan_State_NotInit = 0,
    slcan_State_Active,
    slcan_State_Error
} slcan_tenState;

#define SLCAN_STATUS_RX_FIFO_FULL    ((uint8)0x01u)
#define SLCAN_STATUS_TX_FIFO_FULL    ((uint8)0x02u)
#define SLCAN_STATUS_SJA_ERR_WARNING ((uint8)0x04u)
#define SLCAN_STATUS_SJA_ERR_OVRRUN  ((uint8)0x08u)
#define SLCAN_STATUS_SJA_ERR_PASSIVE ((uint8)0x20u)
#define SLCAN_STATUS_SJA_ERR_ARB_LST ((uint8)0x40u)
#define SLCAN_STATUS_SJA_ERR_BUS_ERR ((uint8)0x80u)

void slcan_Init(const slcan_SerialIfc  *Serial, const slcan_CanIfc  *Can);

void slcan_Deinit(void);

void slcan_OnSerialReceive(uint8 *data,uint16 size);

void slcan_OnSerialTxReady(void);

void slcan_OnCanReceive(uint32 ID, uint16 dlc, uint8 *data);

slcan_tenState slcan_GetState(void);
#endif
