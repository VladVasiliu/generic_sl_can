/*
 * sl_can_internal.h
 *
 *  Created on: Dec 10, 2022
 *      Author: vlad
 */

#ifndef SL_CAN_INTERNAL_H_
#define SL_CAN_INTERNAL_H_

#include "sl_can.h"
#include "sl_can_cfg.h"
#define SLCAN_MAX_DLC 8u
typedef struct
{
    slcan_tenState State;
    uint8 slcan_errorcode;
    uint8 SerialRxBuff[SL_CAN_RX_SERIAL_BUFF];
    uint8 SerialTxCmdBuff[SL_CAN_TX_SERIAL_BUFF];
    uint8 SerialTxRxCanBuff[SL_CAN_TX_SERIAL_BUFF];
    uint8 CanTxBuff[SLCAN_MAX_DLC];
    uint8 CanTxDlc;
    uint8 SerialTxCmdBuffLock;
    uint8 SerialTxRxCanBuffLock;
    uint16 SerialRxCount;
    uint16 SerialTxCmdCount;
    uint16 SerialTxRxCanCount;
    const slcan_SerialIfc *SerialIfc;
    const slcan_CanIfc *CanIfc;
} slcan_tst_internal;

extern slcan_tst_internal slcan_internal;

#endif /* SL_CAN_INTERNAL_H_ */
