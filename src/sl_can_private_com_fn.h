/*
 * sl_can_private_com_fn.h
 *
 *  Created on: Dec 26, 2022
 *      Author: vlad
 */

#ifndef SL_CAN_PRIVATE_COM_FN_H_
#define SL_CAN_PRIVATE_COM_FN_H_

static inline uint8 SLCAN_Lock (uint8 * const lock)
{
    uint8 ok = 0;
    SL_CAN_ENTER_CRITICAL();
    if (*lock == 0)
    {
        (*lock)++;
        ok = 1;
    }
    SL_CAN_EXIT_CRITICAL();
    return ok;
}

static inline void SLCAN_Unlock (uint8 * const lock)
{
    SL_CAN_ENTER_CRITICAL();
    *lock = 0;
    SL_CAN_EXIT_CRITICAL();
}

static inline void ResetRXSerialComm (void)
{
    while (slcan_internal.SerialRxCount > 0)
    {
        slcan_internal.SerialRxBuff[slcan_internal.SerialRxCount] = 0;
        slcan_internal.SerialRxCount--;
    }
    slcan_internal.SerialRxBuff[0] = 0;

}
static inline uint8 ReadCommand (uint8 *ok)
{
    *ok = 0;

    if (sl_can_IsExpectedChar (slcan_internal.SerialRxBuff[slcan_internal.SerialRxCount]) == 0)
    {
        /* ---- Current command is corrupted. waiting for the command is done ----*/
        return 1;
    }

    if (slcan_internal.SerialRxCount >= SL_CAN_RX_SERIAL_BUFF)
    {
        /* ---- Buffer overflow. waiting for the command is done -----*/
        return 1;
    }

    if (slcan_internal.SerialRxBuff[slcan_internal.SerialRxCount] != SLCAN_CR)
    {
        /* ---- Waiting for the end of the command CR ----*/
        slcan_internal.SerialRxCount++;
        return 0;
    }

    /* ---- A new command was detected ----*/
    *ok = 1;
    return 1;
}
static inline uint8 ChkSpaceInTxCmdBuffer (uint16 estimated_size)
{
    uint8 ret = 0;


    if ((slcan_internal.SerialTxCmdCount + estimated_size) < SL_CAN_TX_SERIAL_BUFF)
    {
        ret = 1;
        slcan_internal.slcan_errorcode &= (uint8) (~SLCAN_STATUS_TX_FIFO_FULL);
    }
    else
    {
        slcan_internal.slcan_errorcode |= SLCAN_STATUS_TX_FIFO_FULL;
    }
    return ret;

}

static inline uint8 ChkSpaceInTxRxCanBuffer (uint16 estimated_size)
{
    uint8 ret = 0;


    if ((slcan_internal.SerialTxRxCanCount + estimated_size) < SL_CAN_TX_SERIAL_BUFF)
    {
        ret = 1;
        slcan_internal.slcan_errorcode &= (uint8) (~SLCAN_STATUS_RX_FIFO_FULL);
    }
    else
    {
        /* ---- Reset the buffer and mark the error ----*/
        slcan_internal.slcan_errorcode |= SLCAN_STATUS_RX_FIFO_FULL;
        slcan_internal.SerialTxRxCanCount = 0;
    }

    return ret;

}

static inline void slcan_SendCmdByte (uint8 data)
{

    if (slcan_internal.SerialTxCmdCount < SL_CAN_TX_SERIAL_BUFF)
    {
        slcan_internal.slcan_errorcode &= (uint8) (~SLCAN_STATUS_TX_FIFO_FULL);
    }
    else
    {
        /* ---- Reset the buffer and mark the error ----*/
        slcan_internal.slcan_errorcode |= SLCAN_STATUS_TX_FIFO_FULL;
        slcan_internal.SerialTxCmdCount = 0;
    }

    slcan_internal.SerialTxCmdBuff[slcan_internal.SerialTxCmdCount] = data;
    slcan_internal.SerialTxCmdCount++;

}

static inline void slcan_SendRxCanByte (uint8 data)
{
    if (slcan_internal.SerialTxRxCanCount < SL_CAN_TX_SERIAL_BUFF)
    {
        slcan_internal.slcan_errorcode &= (uint8) (~SLCAN_STATUS_RX_FIFO_FULL);
    }
    else
    {
        /* ---- Reset the buffer and mark the error ----*/
        slcan_internal.slcan_errorcode |= SLCAN_STATUS_RX_FIFO_FULL;
        slcan_internal.SerialTxRxCanCount = 0;
    }

    slcan_internal.SerialTxRxCanBuff[slcan_internal.SerialTxRxCanCount] = data;
    slcan_internal.SerialTxRxCanCount++;

}

static inline void SendNegativeResponse (void)
{
    slcan_SendCmdByte (SLCAN_BELL);
}
static inline void SendPositiveResponse (void)
{
    slcan_SendCmdByte (SLCAN_CR);
}

#endif /* SL_CAN_PRIVATE_COM_FN_H_ */
