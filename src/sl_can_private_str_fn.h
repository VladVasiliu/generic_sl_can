/*
 * sl_can_private_str_fn.h
 *
 *  Created on: Dec 26, 2022
 *      Author: vlad
 */

#ifndef SL_CAN_PRIVATE_STR_FN_H_
#define SL_CAN_PRIVATE_STR_FN_H_



static inline uint8 sl_can_IsExpectedChar (uint8 data)
{
    if (((data >= '0') && (data <= '9')) ||
            ((data >= 'A') && (data <= 'Z')) ||
            ((data >= 'a') && (data <= 'z')) ||
            (data == SLCAN_CR) ||
            (data == SLCAN_BELL))
    {
      return 1;
    }

  return 0;
}
static inline uint8 Str2Nibble (uint8 *str)
{
    uint8 data = 0;

    if ((str[0] >= '0') && (str[0] <= '9'))
    {
        data |= (str[0] - '0');
    }
    if ((str[0] >= 'A') && (str[0] <= 'F'))
    {
        data |= ((str[0] - 'A') + 10);
    }
    if ((str[0] >= 'a') && (str[0] <= 'f'))
    {
        data |= ((str[0] - 'a') + 10);
    }
    return (data & 0xF);
}
static inline uint8 Nibble2Ascii (uint8 nibble)
{
    uint8 ret = 0;

    if (nibble < 0xA)
    {
        ret = '0' + (nibble & 0xF);
    }
    else
    {
        ret = 'A' + ((nibble - 0xA) & 0xF);
    }

    return ret;
}

static inline uint8 Str2uint8 (uint8 *str)
{
    uint8 data = 0;

    data = Str2Nibble (&str[0]) << 4;
    data |= Str2Nibble (&str[1]);

    return data;
}

static inline uint16 GetStdCanID (uint8 *str)
{
    uint16 data = 0;
    data |= Str2Nibble (&str[0]) << 8;
    data |= Str2Nibble (&str[1]) << 4;
    data |= Str2Nibble (&str[2]);
    return data;
}

static inline uint32 GetExtCanID (uint8 *str)
{
    uint32 data = 0;
    data |= (((uint32) Str2Nibble (&str[0])) << 28u);
    data |= (((uint32) Str2Nibble (&str[1])) << 24u);
    data |= (((uint32) Str2Nibble (&str[2])) << 20u);
    data |= (((uint32) Str2Nibble (&str[3])) << 16u);
    data |= (((uint32) Str2Nibble (&str[4])) << 12u);
    data |= (((uint32) Str2Nibble (&str[5])) << 8u);
    data |= (((uint32) Str2Nibble (&str[6])) << 4u);
    data |= ((uint32) Str2Nibble (&str[7]));
    return data;
}

static inline uint32 Str2uint32 (uint8 *str)
{
    uint32 data = 0;
    data |= (((uint32) Str2Nibble (&str[0])) << 28);
    data |= (((uint32) Str2Nibble (&str[1])) << 24);
    data |= (((uint32) Str2Nibble (&str[2])) << 20);
    data |= (((uint32) Str2Nibble (&str[3])) << 16);
    data |= (((uint32) Str2Nibble (&str[4])) << 12);
    data |= (((uint32) Str2Nibble (&str[5])) << 8);
    data |= (((uint32) Str2Nibble (&str[6])) << 4);
    data |= ((uint32) Str2Nibble (&str[7]));
    return data;
}

#endif /* SL_CAN_PRIVATE_STR_FN_H_ */
