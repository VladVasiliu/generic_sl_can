/*
 * sl_can_cfg.h
 *
 *  Created on: Dec 10, 2022
 *      Author: vlad
 */

#ifndef SL_CAN_CFG_H_
#define SL_CAN_CFG_H_


#define SL_CAN_RX_SERIAL_BUFF 256
#define SL_CAN_TX_SERIAL_BUFF 256

#if (SL_CAN_RX_SERIAL_BUFF == 0)
#error "Error : Please configure a RX buffer size > 0"
#endif

#if (SL_CAN_TX_SERIAL_BUFF == 0)
#error "Error : Please configure a TX buffer size > 0"
#endif

#define SLCAN_MAJVER 0x10
#define SLCAN_MINVER 0x13

#define SLCAN_SER_NR0 0x12
#define SLCAN_SER_NR1 0x34

#ifdef MODULE_TEST
#define SL_CAN_ENTER_CRITICAL()
#define SL_CAN_EXIT_CRITICAL()
#else
#include "cmsis_gcc.h"
#define SL_CAN_ENTER_CRITICAL()   __disable_irq ();
#define SL_CAN_EXIT_CRITICAL()    __enable_irq();
#endif


#endif /* SL_CAN_CFG_H_ */
