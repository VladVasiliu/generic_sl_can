/*
 * sl_can_private_cmd_fn.h
 *
 *  Created on: Dec 26, 2022
 *      Author: vlad
 */

#ifndef SL_CAN_PRIVATE_CMD_FN_H_
#define SL_CAN_PRIVATE_CMD_FN_H_

static inline void HandleBaurateCommand (void)
{
    const uint32 Baud[9] = {
            10000, 20000, 50000, 100000, 125000, 250000,
            500000, 800000, 1000000 };
    uint8 index = 0;
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 2)
    {
        index = Str2Nibble (&slcan_internal.SerialRxBuff[1]);

        if (index >= 9)
        {
            SendNegativeResponse ();
        }
        else
        {
            if (((slcan_internal.CanIfc)->SetBaudrate) (Baud[index]) == 0)
            {
                SendNegativeResponse ();
            }
            else
            {
                SendPositiveResponse ();
            }
        }
    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}
static inline void HandleSetupCommand (void)
{
    uint8 BTR0 = 0;
    uint8 BTR1 = 0;

    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 5)
    {
        BTR0 = Str2uint8 (&slcan_internal.SerialRxBuff[1]);
        BTR1 = Str2uint8 (&slcan_internal.SerialRxBuff[3]);
        if ((slcan_internal.CanIfc->SetBtr) (BTR0, BTR1) == 0)
        {
            SendNegativeResponse ();
        }
        else
        {
            SendPositiveResponse ();
        }
    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}

static inline void HandleOpenCommand (void)
{
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 1)
    {
        /* ---- Clear Error codes ----*/
        slcan_internal.slcan_errorcode = 0x00;

        if ((slcan_internal.CanIfc->CanStart) () == 0)
        {
            SendNegativeResponse ();
        }
        else
        {
            SendPositiveResponse ();
        }
    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}

static inline void HandleCloseCommand (void)
{
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 1)
    {
        (slcan_internal.CanIfc->CanStop) ();

        SendPositiveResponse ();
    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}
static inline void HandleStdTxCommand ()
{
    uint32 ID = 0;
    uint8 dlc = 0;
    uint8 dlc_calc = 0;
    uint8 count = 0;
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount >= 5)
    {
        ID = GetStdCanID (&slcan_internal.SerialRxBuff[1]);
        dlc = Str2Nibble (&slcan_internal.SerialRxBuff[4]);
        dlc_calc = (slcan_internal.SerialRxCount - 5) >> 1;

        if (dlc_calc != dlc)
        {
            /* ---- Incorrect format ---- */
            SendNegativeResponse ();
            return;
        }

        for (count = 0; count < dlc; count++)
        {
            slcan_internal.CanTxBuff[count] = Str2uint8 (&slcan_internal.SerialRxBuff[5 + (count << 1)]);
        }

        /* ---- Pad the rest of the buffer with zeros ---- */
        for (; count < SLCAN_MAX_DLC; count++)
        {
            slcan_internal.CanTxBuff[count] = 0;
        }

        slcan_internal.CanTxDlc = dlc;

        if ((slcan_internal.CanIfc->CanTx) (ID, dlc, slcan_internal.CanTxBuff) == 0)
        {
            SendNegativeResponse ();
        }
        else
        {
            SendPositiveResponse ();
        }

    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}

static inline void HandleExtTxCommand ()
{
    uint32 ID = 0;
    uint8 dlc = 0;
    uint8 dlc_calc = 0;
    uint8 count = 0;
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount >= 10)
    {
        ID = GetExtCanID (&slcan_internal.SerialRxBuff[1]);
        dlc = Str2Nibble (&slcan_internal.SerialRxBuff[9]);
        dlc_calc = (slcan_internal.SerialRxCount - 10) >> 1;

        if (dlc_calc != dlc)
        {
            /* ---- Incorrect format ---- */
            SendNegativeResponse ();
            return;
        }

        for (count = 0; count < dlc; count++)
        {
            slcan_internal.CanTxBuff[count] = Str2uint8 (&slcan_internal.SerialRxBuff[10 + (count << 1)]);
        }

        /* ---- Pad the rest of the buffer with zeros ---- */
        for (; count < SLCAN_MAX_DLC; count++)
        {
            slcan_internal.CanTxBuff[count] = 0;
        }

        slcan_internal.CanTxDlc = dlc;
        if ((slcan_internal.CanIfc->CanTx) (ID, dlc, slcan_internal.CanTxBuff) == 0)
        {
            SendNegativeResponse ();
        }
        else
        {
            SendPositiveResponse ();
        }
    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}
static inline void HandleStdRTRTxCommand ()
{
    uint32 ID = 0;
    uint8 dlc = 0;
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 5)
    {
        ID = GetStdCanID (&slcan_internal.SerialRxBuff[1]);
        dlc = Str2Nibble (&slcan_internal.SerialRxBuff[4]);

        if ((slcan_internal.CanIfc->CanTx) (ID | SL_CAN_REMOTE_FRAME, dlc, NULL)
                == 0)
        {
            SendNegativeResponse ();
        }
        else
        {
            SendPositiveResponse ();
        }

    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}

static inline void HandleExtRTRTxCommand ()
{
    uint32 ID = 0;
    uint8 dlc = 0;
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 10)
    {
        ID = GetExtCanID (&slcan_internal.SerialRxBuff[1]);
        dlc = Str2Nibble (&slcan_internal.SerialRxBuff[9]);

        if ((slcan_internal.CanIfc->CanTx) (ID | SL_CAN_REMOTE_FRAME, dlc, NULL) == 0)
        {
            SendNegativeResponse ();
        }
        else
        {
            SendPositiveResponse ();
        }

    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}

static inline void HandleAcceptanceCodeCommand ()
{
    uint32 code = 0;
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 9)
    {
        code = Str2uint32 (&slcan_internal.SerialRxBuff[1]);

        if ((slcan_internal.CanIfc->CanSetCodeRegister) (code) == 0)
        {
            SendNegativeResponse ();
        }
        else
        {
            SendPositiveResponse ();
        }

    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}
static inline void HandleAcceptanceMaskCommand ()
{
    uint32 mask = 0;
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 9)
    {
        mask = Str2uint32 (&slcan_internal.SerialRxBuff[1]);

        if ((slcan_internal.CanIfc->CanSetMaskCodeRegister) (mask) == 0)
        {
            SendNegativeResponse ();
        }
        else
        {
            SendPositiveResponse ();
        }

    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}
static inline void HandleReadFlagsCommand ()
{
    uint8 flags = 0;

    if (ChkSpaceInTxCmdBuffer (4) == 0)
    {
        /* ---- Command cannot be sent. Tx buffer is full   -----*/
        SendNegativeResponse ();

        return;
    }

    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 1)
    {
        flags = slcan_internal.CanIfc->CanReadStatusFlags ();
        flags |= slcan_internal.slcan_errorcode;

        /* ---- Clear Error codes ----*/
        slcan_internal.slcan_errorcode = 0x00;
        slcan_SendCmdByte ('F');
        slcan_SendCmdByte (Nibble2Ascii (flags >> 4));
        slcan_SendCmdByte (Nibble2Ascii (flags & 0xF));
        slcan_SendCmdByte (SLCAN_CR);
    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}
static inline void HandleReadVersionCommand ()
{
    if (ChkSpaceInTxCmdBuffer (6) == 0)
    {
        /* ---- Command cannot be sent. Tx buffer is full   -----*/
        SendNegativeResponse ();

        return;
    }

    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 1)
    {
        slcan_SendCmdByte ('V');
        slcan_SendCmdByte (Nibble2Ascii (SLCAN_MAJVER >> 4));
        slcan_SendCmdByte (Nibble2Ascii (SLCAN_MAJVER & 0xF));
        slcan_SendCmdByte (Nibble2Ascii (SLCAN_MINVER >> 4));
        slcan_SendCmdByte (Nibble2Ascii (SLCAN_MINVER & 0xF));
        slcan_SendCmdByte (SLCAN_CR);
    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}

static inline void HandleReadSerialNrCommand ()
{
    if (ChkSpaceInTxCmdBuffer (6) == 0)
    {
        /* ---- Command cannot be sent. Tx buffer is full   -----*/
        SendNegativeResponse ();

        return;
    }

    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 1)
    {
        slcan_SendCmdByte ('N');
        slcan_SendCmdByte (Nibble2Ascii (SLCAN_SER_NR0 >> 4));
        slcan_SendCmdByte (Nibble2Ascii (SLCAN_SER_NR0 & 0xF));
        slcan_SendCmdByte (Nibble2Ascii (SLCAN_SER_NR1 >> 4));
        slcan_SendCmdByte (Nibble2Ascii (SLCAN_SER_NR1 & 0xF));
        slcan_SendCmdByte (SLCAN_CR);
    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}

static inline void HandleTimeStampCommand (void)
{
    uint8 state = 0;
    /* ---- Check if the command is correctly formated ----*/
    if (slcan_internal.SerialRxCount == 2)
    {
        state = Str2Nibble (&slcan_internal.SerialRxBuff[1]);

        if (state == 0)
        {
            SendPositiveResponse ();
        }
        else
        {
            SendNegativeResponse ();
        }

    }
    else
    {
        /* ---- Command has a wrong formating  -----*/
        SendNegativeResponse ();
    }
}

#endif /* SL_CAN_PRIVATE_CMD_FN_H_ */
