#include "types.h"
#include "sl_can_internal.h"

#define SLCAN_CR   13
#define SLCAN_BELL  7

slcan_tst_internal slcan_internal;

#include "sl_can_private_str_fn.h"

#include "sl_can_private_com_fn.h"

#include "sl_can_private_cmd_fn.h"

static inline void HandleSerialTxCmd (void)
{

    /* ---- Lock buffer to avoid concurrent access  ----*/
    if (SLCAN_Lock (&slcan_internal.SerialTxCmdBuffLock) == 0)
    {
        return;
    }

    if ((((slcan_internal.SerialIfc)->isTxReady ()) != 0) && (slcan_internal.SerialTxCmdCount != 0))
    {
        ((slcan_internal.SerialIfc)->Write) (slcan_internal.SerialTxCmdBuff, slcan_internal.SerialTxCmdCount);
        slcan_internal.SerialTxCmdCount = 0;

    }

    /* ---- Unlock buffer ----*/
    SLCAN_Unlock (&slcan_internal.SerialTxCmdBuffLock);
}

static inline void HandleSerialTxRxCan (void)
{

    /* ---- Lock buffer to avoid concurrent access  ----*/
    if (SLCAN_Lock (&slcan_internal.SerialTxRxCanBuffLock) == 0)
    {
        return;
    }

    if ((((slcan_internal.SerialIfc)->isTxReady ()) != 0) && (slcan_internal.SerialTxRxCanCount != 0))
    {
        ((slcan_internal.SerialIfc)->Write) (slcan_internal.SerialTxRxCanBuff, slcan_internal.SerialTxRxCanCount);
        slcan_internal.SerialTxRxCanCount = 0;

    }

    /* ---- Unlock buffer ----*/
    SLCAN_Unlock (&slcan_internal.SerialTxRxCanBuffLock);

}

static inline void HandleSerialRx (uint8 *data, uint16 size)
{
    uint8 ok = 0;
    uint16 count = 0;

    /* ---- Read all the data from the stream   ----*/
    for (count = 0; count < size; count++)
    {
        slcan_internal.SerialRxBuff[slcan_internal.SerialRxCount] = data[count];

        /* ---- Read the rest of the command ----*/
        if (ReadCommand (&ok) != 0)
        {
            /* ---- Check if  no Rx failure happened  ----*/
            if (ok != 0)
            {
                /* ---- Lock buffer to avoid concurrent access  ----*/
                if (SLCAN_Lock (&slcan_internal.SerialTxCmdBuffLock) != 0)
                {

                    switch (slcan_internal.SerialRxBuff[0])
                    {
                        case 'S':
                        {
                            /* ---- Handle the command ----*/
                            HandleBaurateCommand ();
                        }
                        break;
                        case 's':
                        {
                            /* ---- Handle the command ----*/
                            HandleSetupCommand ();
                        }
                        break;
                        case 'O':
                        {
                            /* ---- Handle the command ----*/
                            HandleOpenCommand ();
                        }
                        break;
                        case 'C':
                        {
                            /* ---- Handle the command ----*/
                            HandleCloseCommand ();
                        }
                        break;
                        case 't':
                        {
                            /* ---- Handle the command ----*/
                            HandleStdTxCommand ();
                        }
                        break;
                        case 'T':
                        {
                            /* ---- Handle the command ----*/
                            HandleExtTxCommand ();
                        }
                        break;
                        case 'r':
                        {
                            /* ---- Handle the command ----*/
                            HandleStdRTRTxCommand ();
                        }
                        break;
                        case 'R':
                        {
                            /* ---- Handle the command ----*/
                            HandleExtRTRTxCommand ();
                        }
                        break;
                        case 'M':
                        {
                            /* ---- Handle the command ----*/
                            HandleAcceptanceCodeCommand ();
                        }
                        break;
                        case 'm':
                        {
                            /* ---- Handle the command ----*/
                            HandleAcceptanceMaskCommand ();
                        }
                        break;
                        case 'F':
                        {
                            /* ---- Handle the command ----*/
                            HandleReadFlagsCommand ();
                        }
                        break;
                        case 'V':
                        case 'v':
                        {
                            /* ---- Handle the command ----*/
                            HandleReadVersionCommand ();
                        }
                        break;
                        case 'N':
                        {
                            /* ---- Handle the command ----*/
                            HandleReadSerialNrCommand ();
                        }
                        break;
                        case 'Z':
                        {
                            /* ---- Handle the command ----*/
                            HandleTimeStampCommand ();
                        }
                        break;
                        /* ---- Stubbed commands ---- */
                        case 'Q': /* Qn[CR] Auto Startup feature (from power on).*/
                        case 'U': /* Un[CR] Setup UART with a new baud rate where n is 0-6. */
                        case 'P': /* P[CR] Poll incomming FIFO for CAN frames (single poll) */
                        case 'A': /* A[CR] Polls incomming FIFO for CAN frames (all pending frames) */
                        case 'W': /* Wn[CR] Filter mode setting */
                        case 'X': /* Xn[CR] Sets Auto Poll/Send ON/OFF for received frames */
                        case 'L': /* Open the CAN channel in listen only mode (receiving)*/
                        case SLCAN_CR:
                        {
                            /* ---- Handle a stubbed command with a positive response---- */
                            SendPositiveResponse ();
                        }
                        break;
                        default:
                        {
                            SendNegativeResponse ();
                        }
                        break;
                    }

                    /* ---- Unlock buffer ----*/
                    SLCAN_Unlock (&slcan_internal.SerialTxCmdBuffLock);
                }
            }

            ResetRXSerialComm ();

            HandleSerialTxCmd ();
        }
    }

}

void slcan_Init (const slcan_SerialIfc *Serial, const slcan_CanIfc *Can)
{
    slcan_internal.State = slcan_State_NotInit;

    slcan_internal.SerialIfc = (const slcan_SerialIfc*) Serial;
    slcan_internal.CanIfc = (const slcan_CanIfc*) Can;

    if ((slcan_internal.SerialIfc == NULL) || (slcan_internal.CanIfc == NULL))
    {
        /* ---- Cannot run serial can without a serial connection -----*/
        return;
    }
    if (((slcan_internal.SerialIfc)->Write == NULL) || ((slcan_internal.SerialIfc)->isTxReady == NULL))
    {
        /* ---- Cannot run serial can without a serial connection -----*/
        return;
    }
    /* ---- Check for a CAN Connection -----*/
    if (((slcan_internal.CanIfc->SetBtr) == NULL)
                    || ((slcan_internal.CanIfc->SetBaudrate) == NULL)
                    || ((slcan_internal.CanIfc->CanStart) == NULL)
                    || ((slcan_internal.CanIfc->CanStop) == NULL)
                    || ((slcan_internal.CanIfc->CanTx) == NULL)
                    || ((slcan_internal.CanIfc->CanSetCodeRegister) == NULL)
                    || ((slcan_internal.CanIfc->CanSetMaskCodeRegister) == NULL)
                    || ((slcan_internal.CanIfc->CanReadStatusFlags) == NULL))
    {
        /* ---- Cannot run serial can without a can connection -----*/
        return;
    }


    /* ---- Reset the RX buffer ----*/
    slcan_internal.SerialRxCount = SL_CAN_RX_SERIAL_BUFF - 1;
    ResetRXSerialComm ();

    /* ---- Reset internal data -----*/
    SL_CAN_ENTER_CRITICAL();
    slcan_internal.slcan_errorcode = 0;
    slcan_internal.SerialTxCmdCount = 0;
    slcan_internal.SerialTxRxCanCount = 0;
    slcan_internal.SerialTxRxCanBuffLock = 0;
    slcan_internal.SerialTxCmdBuffLock = 0;
    slcan_internal.CanTxDlc = 0;

    /* ---- All the interfaces are properly defined ----*/
    slcan_internal.State = slcan_State_Active;

    SL_CAN_EXIT_CRITICAL();
}

void slcan_Deinit (void)
{
    slcan_internal.State = slcan_State_NotInit;
    slcan_internal.SerialIfc = NULL;
    slcan_internal.CanIfc = NULL;
}

void slcan_OnSerialReceive (uint8 *data, uint16 size)
{

    if ((data == NULL) || (size == 0))
    {
        return;
    }

    /* ---- Handle the RX stream of data ----*/
    HandleSerialRx (data, size);

}
void slcan_OnSerialTxReady (void)
{
    HandleSerialTxCmd ();
    HandleSerialTxRxCan ();
}

static inline void slcan_OnFrameReceive (uint32 ID, uint16 dlc, uint8 *data)
{
    uint8 count = 0;
    uint8 size = 10;
    /* ---- Calculate buffer size ---- */
    size += dlc * 2;

    /* ---- Check if the frame can be sent ---- */
    if (ChkSpaceInTxRxCanBuffer (size) == 0)
    {
        /* ---- Command cannot be sent. Tx buffer is full. Rx cannot be performed  -----*/
        return;
    }

    /* ---- Lock buffer to avoid concurrent access  ----*/
    if (SLCAN_Lock (&slcan_internal.SerialTxRxCanBuffLock) == 0)
    {
        return;
    }

    /* ---- Mask the Remote flag -----*/
    ID &= (~SL_CAN_REMOTE_FRAME);

    if (ID > 0x7FF)
    {
        /* ---- This is an extended frame ---- */
        slcan_SendRxCanByte ('T');
        /* ---- Add the ID ---- */
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 28) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 24) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 20) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 16) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 12) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 8) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 4) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 0) & 0xF));
    }
    else
    {
        /* ---- This is a standard frame -----*/
        slcan_SendRxCanByte ('t');
        /* ---- Add the ID ---- */
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 8) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 4) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 0) & 0xF));
    }

    /* ---- Add the DLC ---- */
    slcan_SendRxCanByte (Nibble2Ascii (dlc & 0xF));

    for (count = 0; count < dlc; count++)
    {
        slcan_SendRxCanByte (Nibble2Ascii (data[count] >> 4));
        slcan_SendRxCanByte (Nibble2Ascii (data[count] & 0xF));
    }
    slcan_SendRxCanByte (SLCAN_CR);

    /* ---- Unlock buffer ----*/
    SLCAN_Unlock (&slcan_internal.SerialTxRxCanBuffLock);
}

static inline void slcan_OnFrameRemoteReceive (uint32 ID, uint16 dlc)
{
    /* ---- Check if the frame can be sent ---- */
    if (ChkSpaceInTxRxCanBuffer (10) == 0)
    {
        return;
    }

    /* ---- Lock buffer to avoid concurrent access  ----*/
    if (SLCAN_Lock (&slcan_internal.SerialTxRxCanBuffLock) == 0)
    {
        return;
    }

    /* ---- Mask the Remote flag -----*/
    ID &= (~SL_CAN_REMOTE_FRAME);

    if (ID > 0x7FF)
    {
        /* ---- This is an extended frame ---- */
        slcan_SendRxCanByte ('R');
        /* ---- Add the ID ---- */
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 28) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 24) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 20) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 16) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 12) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 8) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 4) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 0) & 0xF));
    }
    else
    {
        /* ---- This is a standard frame -----*/
        slcan_SendRxCanByte ('r');
        /* ---- Add the ID ---- */
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 8) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 4) & 0xF));
        slcan_SendRxCanByte (Nibble2Ascii ((ID >> 0) & 0xF));
    }

    /* ---- Add the DLC ---- */
    slcan_SendRxCanByte (Nibble2Ascii (dlc & 0xF));
    slcan_SendRxCanByte (SLCAN_CR);
    /* ---- Unlock buffer ----*/
    SLCAN_Unlock (&slcan_internal.SerialTxRxCanBuffLock);
}

void slcan_OnCanReceive (uint32 ID, uint16 dlc, uint8 *data)
{
    if (slcan_internal.State != slcan_State_Active)
    {
        return;
    }

    if ((ID & SL_CAN_REMOTE_FRAME) == SL_CAN_REMOTE_FRAME)
    {
        /* ---- This is a remote frame -----*/
        slcan_OnFrameRemoteReceive (ID, dlc);
    }
    else
    {
        /* ---- This is a standard or extended frame ----*/
        slcan_OnFrameReceive (ID, dlc, data);
    }

    HandleSerialTxRxCan ();
}

slcan_tenState slcan_GetState (void)
{
    return slcan_internal.State;
}
