#include "unity.h"
#include "types.h"
#include "sl_can.h"
#include "Test_Environment.h"

/******************************************************************************
 * TESTS
 ******************************************************************************/
void test_WrongInitialization ()
{
    slcan_SerialIfc Serial =
            {
                    .Write = NULL,
                    .isTxReady = NULL,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = NULL,
                    .CanStop = NULL,
                    .CanTx = NULL,
                    .SetBaudrate = NULL,
                    .SetBtr = NULL,
                    .CanSetCodeRegister = NULL,
                    .CanSetMaskCodeRegister = NULL,
                    .CanReadStatusFlags = NULL
            };

    slcan_Init (NULL, NULL);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    slcan_Init (&Serial, NULL);
    slcan_OnSerialReceive(NULL, 1);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    slcan_Init (NULL, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

}

void test_CorrectInitialization (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = NULL,
                    .isTxReady = NULL
            };
    slcan_CanIfc Can =
            {
                    .CanStart = NULL,
                    .CanStop = NULL,
                    .CanTx = NULL,
                    .SetBaudrate = NULL,
                    .SetBtr = NULL,
                    .CanSetCodeRegister = NULL,
                    .CanSetMaskCodeRegister = NULL,
                    .CanReadStatusFlags = NULL };

    Serial.Write = test_Serial_Write;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    Serial.isTxReady = test_Serial_isTxReady;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 1);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());


    Can.CanStart = test_Can_CanStart;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    Can.CanStop = test_Can_CanStop;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    Can.CanTx = test_Can_CanTx;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    Can.SetBaudrate = test_Can_SetBaudrate;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    Can.SetBtr = test_Can_SetBtr;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    Can.CanSetCodeRegister = test_Can_CanSetCodeRegister;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    Can.CanSetMaskCodeRegister = test_Can_CanSetMaskCodeRegister;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(NULL, 0);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

    Can.CanReadStatusFlags = test_Can_CanReadStatusFlags;
    slcan_Init (&Serial, &Can);
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    slcan_OnCanReceive (0, 0, 0);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    slcan_Deinit ();
    TEST_ASSERT_EQUAL(slcan_State_NotInit, slcan_GetState ());

}

void test_SetStandardBitrate (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };

    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    /* during the test the baudrate can be changed*/
    test_Can_SetBaudrate_ret = 1;
    test_Serial_Read_size = 3;

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S0\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(10000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;

    strncpy ((char*) test_Serial_Read_buff, "S1\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(20000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S2\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(50000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S3\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(100000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S4\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(125000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S5\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(250000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S6\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(500000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S7\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(800000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S8\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(1000000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S9\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(1000000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;

    strncpy ((char*) test_Serial_Read_buff, "S\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;

    strncpy ((char*) test_Serial_Read_buff, "S100\r", sizeof(test_Serial_Read_buff));

    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

}
void test_SetStandardBitrateResponse (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    /* during the test the baudrate can be changed*/
    test_Can_SetBaudrate_ret = 1;
    test_Serial_Read_size = 3;

    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "S0\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(10000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    /* during the test the baudrate cannot be changed*/
    test_Can_SetBaudrate_ret = 0;
    test_Serial_Read_size = 3;

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S0\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(10000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    /* during the test the baudrate can be changed*/
    test_Can_SetBaudrate_ret = 1;
    test_Serial_Read_size = 3;

    test_Serial_isTxReady_ret = 0;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "S0\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(10000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(0, test_Serial_Write_cnt);

    test_Serial_isTxReady_ret = 1;
    slcan_OnSerialTxReady();
    TEST_ASSERT_EQUAL(10000, test_Can_SetBaudrate_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}
void test_SetBTRCommand (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Can_SetBtr_ret = 1;
    test_Serial_Read_size = 6;

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "sA1B2\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(0xA1, test_Can_SetBtr_btr0_par);
    TEST_ASSERT_EQUAL(0xB2, test_Can_SetBtr_btr1_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Can_SetBtr_ret = 0;
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "s039b\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(0x03, test_Can_SetBtr_btr0_par);
    TEST_ASSERT_EQUAL(0x9b, test_Can_SetBtr_btr1_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Can_SetBtr_ret = 1;
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "sa39b\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(0xa3, test_Can_SetBtr_btr0_par);
    TEST_ASSERT_EQUAL(0x9b, test_Can_SetBtr_btr1_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Can_SetBtr_ret = 1;
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "s123\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(0xa3, test_Can_SetBtr_btr0_par);
    TEST_ASSERT_EQUAL(0x9b, test_Can_SetBtr_btr1_par);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}

void test_SetOPENCommand (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Can_CanStart_ret = 0;
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "O\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(1, test_Can_CanStart_cnt);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Can_CanStart_ret = 1;
    test_Can_CanStart_cnt = 0;
    test_Serial_Read_size = 4;

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "O5\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(0, test_Can_CanStart_cnt);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Can_CanStart_ret = 1;
    test_Can_CanStart_cnt = 0;
    test_Serial_Read_size = 2;

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "O\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(1, test_Can_CanStart_cnt);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

}

void test_SetCLOSECommand (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Can_CanStop_cnt = 0;
    test_Serial_Read_size = 2;

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "C\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(1, test_Can_CanStop_cnt);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "C\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(2, test_Can_CanStop_cnt);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Can_CanStop_cnt = 0;
    test_Serial_Read_size = 4;

    test_Serial_isTxReady_ret = 1;
    test_Serial_Write_cnt = 0;
    strncpy ((char*) test_Serial_Read_buff, "C2\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

}
void test_TransmitStdFrame (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "t123411223344\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x123, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x4, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0x11, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0x22, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0x33, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0x44, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "t7548AABBCCDD11223344\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x754, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x8, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0xAA, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0xBB, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0xCC, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0xDD, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(0x11, test_Can_CanTx_data_par[4]);
    TEST_ASSERT_EQUAL(0x22, test_Can_CanTx_data_par[5]);
    TEST_ASSERT_EQUAL(0x33, test_Can_CanTx_data_par[6]);
    TEST_ASSERT_EQUAL(0x44, test_Can_CanTx_data_par[7]);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 0;

    strncpy ((char*) test_Serial_Read_buff, "t3330\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x333, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x0, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[4]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[5]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[6]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[7]);
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "t3330\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x333, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x0, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[4]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[5]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[6]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[7]);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}

void test_TransmitExtFrame (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "T12341234411223344\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x12341234, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x4, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0x11, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0x22, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0x33, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0x44, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "TABCDEF128AABBCCDD11223344\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0xABCDEF12, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x8, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0xAA, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0xBB, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0xCC, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0xDD, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(0x11, test_Can_CanTx_data_par[4]);
    TEST_ASSERT_EQUAL(0x22, test_Can_CanTx_data_par[5]);
    TEST_ASSERT_EQUAL(0x33, test_Can_CanTx_data_par[6]);
    TEST_ASSERT_EQUAL(0x44, test_Can_CanTx_data_par[7]);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 0;

    strncpy ((char*) test_Serial_Read_buff, "T432112340\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x43211234, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x0, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[4]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[5]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[6]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[7]);
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "T545467670\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x54546767, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x0, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[4]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[5]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[6]);
    TEST_ASSERT_EQUAL(0x00, test_Can_CanTx_data_par[7]);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "T5454670\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "T545467674322\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}

void test_TransmitStdRTRFrame (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "r1234\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x123|SL_CAN_REMOTE_FRAME, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x4, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "r7548\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x754|SL_CAN_REMOTE_FRAME, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x8, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 0;

    strncpy ((char*) test_Serial_Read_buff, "r3330\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x333|SL_CAN_REMOTE_FRAME, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x0, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "r3330\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x333|SL_CAN_REMOTE_FRAME, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x0, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "r330\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}

void test_TransmitExtRTRFrame (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "R123412344\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x12341234 | SL_CAN_REMOTE_FRAME, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x4, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "R7BCDEF128\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x7BCDEF12 |SL_CAN_REMOTE_FRAME, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x8, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 0;

    strncpy ((char*) test_Serial_Read_buff, "R432112340\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x43211234|SL_CAN_REMOTE_FRAME, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x0, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "R545467670\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x54546767|SL_CAN_REMOTE_FRAME, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x0, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "R545460\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}
void test_TransmitSetAcceptanceCodeReg (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanSetCodeRegister_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "M12345678\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x12345678, test_Can_CanSetCodeRegister_code_par);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanSetCodeRegister_ret = 0;
    strncpy ((char*) test_Serial_Read_buff, "MABCDEF12\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0xABCDEF12, test_Can_CanSetCodeRegister_code_par);
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanSetCodeRegister_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "M87654321\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x87654321, test_Can_CanSetCodeRegister_code_par);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanSetCodeRegister_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "M1234567\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}

void test_TransmitSetAcceptanceMaskReg (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanSetMaskCodeRegister_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "m12345678\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x12345678, test_Can_CanSetMaskCodeRegister_mask_par);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanSetMaskCodeRegister_ret = 0;
    strncpy ((char*) test_Serial_Read_buff, "mABCDEF12\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0xABCDEF12, test_Can_CanSetMaskCodeRegister_mask_par);
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanSetMaskCodeRegister_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "m87654321\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x87654321, test_Can_CanSetMaskCodeRegister_mask_par);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanSetMaskCodeRegister_ret = 0;
    strncpy ((char*) test_Serial_Read_buff, "m123\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}

void test_TransmitReadFlagsRequest (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanReadStatusFlags_ret = 0x12;
    strncpy ((char*) test_Serial_Read_buff, "F\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL('F', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('2', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL(4, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanReadStatusFlags_ret = 0xA6;
    strncpy ((char*) test_Serial_Read_buff, "F\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL('F', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('6', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL(4, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanReadStatusFlags_ret = 0x7c;
    strncpy ((char*) test_Serial_Read_buff, "F\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL('F', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('7', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('C', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL(4, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 0xAB;
    test_Can_CanReadStatusFlags_ret = 0x7c;
    strncpy ((char*) test_Serial_Read_buff, "F0\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanReadStatusFlags_ret = 0xAB;
    strncpy ((char*) test_Serial_Read_buff, "F\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL('F', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('B', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL(4, test_Serial_Write_cnt);
}

void test_TransmitReadVersionRequest (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "V\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL('V', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('3', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[5]);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "V3\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

}

void test_TransmitReadSerialNrRequest (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "N\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL('N', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('2', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('3', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[5]);
    TEST_ASSERT_EQUAL(6, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "N22\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

}
void test_TransmitControlTimestampRequest (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "Z0\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "Z1\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "Z23\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    strncpy ((char*) test_Serial_Read_buff, "Z\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

}

/**************************************************************************
 * Wrong formated commands
 **************************************************************************/
void test_TransmitWrong1StdFrame (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "t123411223344\r", sizeof(test_Serial_Read_buff));
    test_Serial_Read_buff[1] = 1;
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_NOT_EQUAL(0x4, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "t7548AABBCCDD11223344\r", sizeof(test_Serial_Read_buff));
    test_Serial_Read_buff[5] = 1;
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_NOT_EQUAL(0x754, test_Can_CanTx_ID_par);
    TEST_ASSERT_NOT_EQUAL(0x8, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);


    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "t3335\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_NOT_EQUAL(0x333, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);


    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanTx_ret = 1;

    strncpy ((char*) test_Serial_Read_buff, "t123\r", sizeof(test_Serial_Read_buff));
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_NOT_EQUAL(0x123, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(TEST_BELL, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);
}
void test_ChkCmds (void)
{
    UnitySetTestFile (__FILE__);
    RUN_TEST(test_WrongInitialization);
    RUN_TEST(test_CorrectInitialization);
    RUN_TEST(test_SetStandardBitrate);
    RUN_TEST(test_SetStandardBitrateResponse);
    RUN_TEST(test_SetBTRCommand);
    RUN_TEST(test_SetOPENCommand);
    RUN_TEST(test_SetCLOSECommand);
    RUN_TEST(test_TransmitStdFrame);
    RUN_TEST(test_TransmitExtFrame);
    RUN_TEST(test_TransmitStdRTRFrame);
    RUN_TEST(test_TransmitExtRTRFrame);
    RUN_TEST(test_TransmitSetAcceptanceCodeReg);
    RUN_TEST(test_TransmitSetAcceptanceMaskReg);
    RUN_TEST(test_TransmitReadFlagsRequest);
    RUN_TEST(test_TransmitReadVersionRequest);
    RUN_TEST(test_TransmitReadSerialNrRequest);
    RUN_TEST(test_TransmitControlTimestampRequest);
    RUN_TEST(test_TransmitWrong1StdFrame);
}
