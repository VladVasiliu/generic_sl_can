/*
 * Test_Environment.h
 *
 *  Created on: Dec 27, 2022
 *      Author: vlad
 */

#ifndef TEST_ENVIRONMENT_H_
#define TEST_ENVIRONMENT_H_


#include <string.h>
#include "types.h"
#include "sl_can.h"

#define TEST_CR 13
#define TEST_BELL 7


extern uint8 test_Serial_Read_buff[512];
extern uint8 test_Serial_Write_buff[512];
extern uint16 test_Serial_Read_size;
extern uint16 test_Serial_Write_size;
extern uint16 test_Serial_Read_cnt ;
extern uint16 test_Serial_Write_cnt ;
extern uint8 test_Serial_isTxReady_ret;
extern uint8 test_Serial_isDataAvailable_ret;
extern uint8 test_Can_CanStart_ret;
extern uint32 test_Can_CanTx_ID_par;
extern uint8 test_Can_CanTx_dlc_par ;
extern uint8 *test_Can_CanTx_data_par;
extern uint8 test_Can_CanTx_ret;
extern uint32 test_Can_SetBaudrate_par;
extern uint8 test_Can_SetBaudrate_ret;
extern uint8 test_Can_SetBtr_btr0_par;
extern uint8 test_Can_SetBtr_btr1_par;
extern uint8 test_Can_SetBtr_ret;
extern uint8 test_Can_CanStart_cnt;
extern uint8 test_Can_CanStop_cnt;

extern uint32 test_Can_CanSetCodeRegister_code_par;
extern uint8 test_Can_CanSetCodeRegister_ret;
extern uint32 test_Can_CanSetMaskCodeRegister_mask_par;
extern uint8 test_Can_CanSetMaskCodeRegister_ret ;
extern uint8 test_Can_CanReadStatusFlags_ret;


extern void setUp (void);
extern void tearDown (void);

extern void test_Serial_Write (uint8 *data, uint16 size);
extern uint8 test_Serial_isTxReady (void);

extern uint8 test_Can_CanStart (void);
extern void test_Can_CanStop (void);
extern uint8 test_Can_CanTx (uint32 ID, uint8 dlc, uint8 *data);
extern uint8 test_Can_SetBaudrate (uint32 Baud);
extern uint8 test_Can_SetBtr (uint8 btr0, uint8 btr1);
extern uint8 test_Can_CanSetCodeRegister (uint32 code);
extern uint8 test_Can_CanSetMaskCodeRegister (uint32 mask);
extern uint8 test_Can_CanReadStatusFlags (void);
#endif /* TEST_ENVIRONMENT_H_ */
