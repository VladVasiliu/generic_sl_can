/*
 * Test_Environment.c
 *
 *  Created on: Dec 27, 2022
 *      Author: vlad
 */

#include "Test_Environment.h"

uint8 test_Serial_Read_buff[512];
uint8 test_Serial_Write_buff[512];
uint16 test_Serial_Read_size = 0;
uint16 test_Serial_Write_size = 0;
uint16 test_Serial_Write_cnt = 0;
uint8 test_Serial_isTxReady_ret = 0;


uint8 test_Can_CanStart_ret = 0;
uint32 test_Can_CanTx_ID_par = 0;
uint8 test_Can_CanTx_dlc_par = 0;
uint8 *test_Can_CanTx_data_par = NULL;
uint8 test_Can_CanTx_ret = 0;
uint32 test_Can_SetBaudrate_par = 0;
uint8 test_Can_SetBaudrate_ret = 0;
uint8 test_Can_SetBtr_btr0_par = 0;
uint8 test_Can_SetBtr_btr1_par = 0;
uint8 test_Can_SetBtr_ret = 0;
uint8 test_Can_CanStart_cnt = 0;
uint8 test_Can_CanStop_cnt = 0;

uint32 test_Can_CanSetCodeRegister_code_par = 0;
uint8 test_Can_CanSetCodeRegister_ret = 0;
uint32 test_Can_CanSetMaskCodeRegister_mask_par = 0;
uint8 test_Can_CanSetMaskCodeRegister_ret = 0;
uint8 test_Can_CanReadStatusFlags_ret = 0;

void setUp (void)
{
    memset (test_Serial_Read_buff, 0, sizeof(test_Serial_Read_buff));
    memset (test_Serial_Write_buff, 0, sizeof(test_Serial_Write_buff));
    test_Serial_Read_size = 0;
    test_Serial_Write_size = 0;
    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 0;
    test_Can_CanStart_ret = 0;
    test_Can_CanTx_ID_par = 0;
    test_Can_CanTx_dlc_par = 0;
    test_Can_CanTx_data_par = NULL;
    test_Can_CanTx_ret = 0;
    test_Can_SetBaudrate_par = 0;
    test_Can_SetBaudrate_ret = 0;

    test_Can_SetBtr_btr0_par = 0;
    test_Can_SetBtr_btr1_par = 0;
    test_Can_SetBtr_ret = 0;
    test_Can_CanStart_cnt = 0;
    test_Can_CanStop_cnt = 0;

    test_Can_CanSetCodeRegister_code_par = 0;
    test_Can_CanSetCodeRegister_ret = 0;
    test_Can_CanSetMaskCodeRegister_mask_par = 0;
    test_Can_CanSetMaskCodeRegister_ret = 0;
    test_Can_CanReadStatusFlags_ret = 0;

}
void tearDown (void)
{
    slcan_Deinit ();
}
/******************************************************************************
 * CALLBACKS FOR THE INTERFACES
 ******************************************************************************/

void test_Serial_Write (uint8 *data, uint16 size)
{
    for (int cnt = 0; cnt < size; cnt++)
    {

        if (test_Serial_Write_cnt < sizeof(test_Serial_Write_buff))
        {
            test_Serial_Write_buff[test_Serial_Write_cnt] = data[cnt];
            test_Serial_Write_cnt++;
        }
        else
        {
            break;
        }
    }
}
uint8 test_Serial_isTxReady (void)
{
    return test_Serial_isTxReady_ret;
}



uint8 test_Can_CanStart (void)
{
    test_Can_CanStart_cnt++;
    return test_Can_CanStart_ret;
}
void test_Can_CanStop (void)
{
    test_Can_CanStop_cnt++;
}
uint8 test_Can_CanTx (uint32 ID, uint8 dlc, uint8 *data)
{
    test_Can_CanTx_ID_par = ID;
    test_Can_CanTx_dlc_par = dlc;
    test_Can_CanTx_data_par = data;
    return test_Can_CanTx_ret;
}
uint8 test_Can_SetBaudrate (uint32 Baud)
{
    test_Can_SetBaudrate_par = Baud;
    return test_Can_SetBaudrate_ret;
}
uint8 test_Can_SetBtr (uint8 btr0, uint8 btr1)
{
    test_Can_SetBtr_btr0_par = btr0;
    test_Can_SetBtr_btr1_par = btr1;
    return test_Can_SetBtr_ret;
}

uint8 test_Can_CanSetCodeRegister (uint32 code)
{
    test_Can_CanSetCodeRegister_code_par = code;
    return test_Can_CanSetCodeRegister_ret;
}
uint8 test_Can_CanSetMaskCodeRegister (uint32 mask)
{
    test_Can_CanSetMaskCodeRegister_mask_par = mask;
    return test_Can_CanSetMaskCodeRegister_ret;
}
uint8 test_Can_CanReadStatusFlags (void)
{
    return test_Can_CanReadStatusFlags_ret;
}
