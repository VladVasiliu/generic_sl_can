/*
 * Test_Communication.c
 *
 *  Created on: Dec 27, 2022
 *      Author: vlad
 */

#include "unity.h"
#include "types.h"
#include "sl_can.h"
#include "Test_Environment.h"
#include "sl_can_internal.h"

void test_Overflow_OnCanTx (void)
{
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    strncpy ((char*) test_Serial_Read_buff, "t123411223344\r", sizeof(test_Serial_Read_buff));
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_isTxReady_ret = 0;
    test_Can_CanTx_ret = 1;

    for (int i = 0; i < 512; i++)
    {

        slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    }
    test_Serial_isTxReady_ret = 1;
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(0x123, test_Can_CanTx_ID_par);
    TEST_ASSERT_EQUAL(0x4, test_Can_CanTx_dlc_par);
    TEST_ASSERT_EQUAL(0x11, test_Can_CanTx_data_par[0]);
    TEST_ASSERT_EQUAL(0x22, test_Can_CanTx_data_par[1]);
    TEST_ASSERT_EQUAL(0x33, test_Can_CanTx_data_par[2]);
    TEST_ASSERT_EQUAL(0x44, test_Can_CanTx_data_par[3]);
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);

}
void test_NormalRxStdFrame (void)
{
    uint8 testdata[8] = {
            1, 2, 3, 4, 5, 6, 7, 8 };
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    /* ---- PRECONDITION ----*/
    strncpy ((char*) test_Serial_Read_buff, "O\r", sizeof(test_Serial_Read_buff));
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Can_CanTx_ret = 1;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanStart_ret = 1;
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    slcan_OnCanReceive (0x123, 8, testdata);
    slcan_OnSerialTxReady();

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(22, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('t', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('2', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('3', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('8', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[5]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[6]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[7]);
    TEST_ASSERT_EQUAL('2', test_Serial_Write_buff[8]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[9]);
    TEST_ASSERT_EQUAL('3', test_Serial_Write_buff[10]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[11]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[12]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[13]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[14]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[15]);
    TEST_ASSERT_EQUAL('6', test_Serial_Write_buff[16]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[17]);
    TEST_ASSERT_EQUAL('7', test_Serial_Write_buff[18]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[19]);
    TEST_ASSERT_EQUAL('8', test_Serial_Write_buff[20]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[21]);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    testdata[0] = 0xAA;
    testdata[1] = 0xBB;
    testdata[2] = 0xCC;
    testdata[3] = 0xDD;

    slcan_OnCanReceive (0x555, 4, testdata);
    slcan_OnSerialTxReady();

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(14, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('t', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[5]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[6]);
    TEST_ASSERT_EQUAL('B', test_Serial_Write_buff[7]);
    TEST_ASSERT_EQUAL('B', test_Serial_Write_buff[8]);
    TEST_ASSERT_EQUAL('C', test_Serial_Write_buff[9]);
    TEST_ASSERT_EQUAL('C', test_Serial_Write_buff[10]);
    TEST_ASSERT_EQUAL('D', test_Serial_Write_buff[11]);
    TEST_ASSERT_EQUAL('D', test_Serial_Write_buff[12]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[13]);

}

void test_NormalRxExtFrame (void)
{
    uint8 testdata[8] = {
            1, 2, 3, 4, 5, 6, 7, 8 };
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    /* ---- PRECONDITION ----*/
    strncpy ((char*) test_Serial_Read_buff, "O\r", sizeof(test_Serial_Read_buff));
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Can_CanTx_ret = 1;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanStart_ret = 1;
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    slcan_OnCanReceive (0x12345678, 8, testdata);
    slcan_OnSerialTxReady();

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(27, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('T', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('2', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('3', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[5]);
    TEST_ASSERT_EQUAL('6', test_Serial_Write_buff[6]);
    TEST_ASSERT_EQUAL('7', test_Serial_Write_buff[7]);
    TEST_ASSERT_EQUAL('8', test_Serial_Write_buff[8]);
    TEST_ASSERT_EQUAL('8', test_Serial_Write_buff[9]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[10]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[11]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[12]);
    TEST_ASSERT_EQUAL('2', test_Serial_Write_buff[13]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[14]);
    TEST_ASSERT_EQUAL('3', test_Serial_Write_buff[15]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[16]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[17]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[18]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[19]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[20]);
    TEST_ASSERT_EQUAL('6', test_Serial_Write_buff[21]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[22]);
    TEST_ASSERT_EQUAL('7', test_Serial_Write_buff[23]);
    TEST_ASSERT_EQUAL('0', test_Serial_Write_buff[24]);
    TEST_ASSERT_EQUAL('8', test_Serial_Write_buff[25]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[26]);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    testdata[0] = 0xAA;
    testdata[1] = 0xBB;
    testdata[2] = 0xCC;
    testdata[3] = 0xDD;

    slcan_OnCanReceive (0x5555AAAA, 4, testdata);
    slcan_OnSerialTxReady();

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(19, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('T', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[5]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[6]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[7]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[8]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[9]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[10]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[11]);
    TEST_ASSERT_EQUAL('B', test_Serial_Write_buff[12]);
    TEST_ASSERT_EQUAL('B', test_Serial_Write_buff[13]);
    TEST_ASSERT_EQUAL('C', test_Serial_Write_buff[14]);
    TEST_ASSERT_EQUAL('C', test_Serial_Write_buff[15]);
    TEST_ASSERT_EQUAL('D', test_Serial_Write_buff[16]);
    TEST_ASSERT_EQUAL('D', test_Serial_Write_buff[17]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[18]);
}

void test_NormalRxStdRTRFrame (void)
{
    uint8 testdata[8] = {
            1, 2, 3, 4, 5, 6, 7, 8 };
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    /* ---- PRECONDITION ----*/
    strncpy ((char*) test_Serial_Read_buff, "O\r", sizeof(test_Serial_Read_buff));
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Can_CanTx_ret = 1;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanStart_ret = 1;
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    slcan_OnCanReceive (0x123 | SL_CAN_REMOTE_FRAME, 8, testdata);
    slcan_OnSerialReceive((uint8 *)" ", 1);

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(6, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('r', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('2', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('3', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('8', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[5]);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    testdata[0] = 0xAA;
    testdata[1] = 0xBB;
    testdata[2] = 0xCC;
    testdata[3] = 0xDD;

    slcan_OnCanReceive (0x555 | SL_CAN_REMOTE_FRAME, 4, testdata);
    slcan_OnSerialTxReady();

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(6, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('r', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[5]);

}


void test_NormalRxExtRTRFrame (void)
{
    uint8 testdata[8] = {
            1, 2, 3, 4, 5, 6, 7, 8 };
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    /* ---- PRECONDITION ----*/
    strncpy ((char*) test_Serial_Read_buff, "O\r", sizeof(test_Serial_Read_buff));
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Can_CanTx_ret = 1;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanStart_ret = 1;
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    slcan_OnCanReceive (0x12345678 | SL_CAN_REMOTE_FRAME, 8, testdata);
    slcan_OnSerialTxReady();

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(11, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('R', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('1', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('2', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('3', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[5]);
    TEST_ASSERT_EQUAL('6', test_Serial_Write_buff[6]);
    TEST_ASSERT_EQUAL('7', test_Serial_Write_buff[7]);
    TEST_ASSERT_EQUAL('8', test_Serial_Write_buff[8]);
    TEST_ASSERT_EQUAL('8', test_Serial_Write_buff[9]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[10]);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    testdata[0] = 0xAA;
    testdata[1] = 0xBB;
    testdata[2] = 0xCC;
    testdata[3] = 0xDD;

    slcan_OnCanReceive (0x55AA55AA | SL_CAN_REMOTE_FRAME, 4, testdata);
    slcan_OnSerialTxReady();

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(11, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('R', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[5]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[6]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[7]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[8]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[9]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[10]);

}


void test_OverflowOnCANRx (void)
{
    uint8 testdata[8] = {
            1, 2, 3, 4, 5, 6, 7, 8 };
    slcan_SerialIfc Serial =
            {
                    .Write = &test_Serial_Write,
                    .isTxReady = &test_Serial_isTxReady,
            };
    slcan_CanIfc Can =
            {
                    .CanStart = &test_Can_CanStart,
                    .CanStop = &test_Can_CanStop,
                    .CanTx = &test_Can_CanTx,
                    .SetBaudrate = &test_Can_SetBaudrate,
                    .SetBtr = &test_Can_SetBtr,
                    .CanSetCodeRegister = &test_Can_CanSetCodeRegister,
                    .CanSetMaskCodeRegister = &test_Can_CanSetMaskCodeRegister,
                    .CanReadStatusFlags = &test_Can_CanReadStatusFlags,
            };
    slcan_Init (&Serial, &Can);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());

    /* ---- PRECONDITION ----*/
    strncpy ((char*) test_Serial_Read_buff, "O\r", sizeof(test_Serial_Read_buff));
    test_Serial_Read_size = sizeof(test_Serial_Read_buff);

    test_Serial_Write_cnt = 0;
    test_Can_CanTx_ret = 1;
    test_Serial_isTxReady_ret = 1;
    test_Can_CanStart_ret = 1;
    slcan_OnSerialReceive(test_Serial_Read_buff, test_Serial_Read_size);
    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(TEST_CR, test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL(1, test_Serial_Write_cnt);

    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    test_Serial_isTxReady_ret = 0;
    for(int i=0;i<256;i++)
    {
        slcan_OnCanReceive (0x123, 8, testdata);
        if(slcan_internal.slcan_errorcode == SLCAN_STATUS_RX_FIFO_FULL) break;
    }

    TEST_ASSERT_EQUAL(SLCAN_STATUS_RX_FIFO_FULL,slcan_internal.slcan_errorcode);
    test_Serial_isTxReady_ret = 1;
    /* ---- TEST Standard Can ---- */
    test_Serial_Write_cnt = 0;
    testdata[0] = 0xAA;
    testdata[1] = 0xBB;
    testdata[2] = 0xCC;
    testdata[3] = 0xDD;

    slcan_OnCanReceive (0x55AA55AA | SL_CAN_REMOTE_FRAME, 4, testdata);
    slcan_OnSerialTxReady();

    TEST_ASSERT_EQUAL(slcan_State_Active, slcan_GetState ());
    TEST_ASSERT_EQUAL(11, test_Serial_Write_cnt);
    TEST_ASSERT_EQUAL('R', test_Serial_Write_buff[0]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[1]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[2]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[3]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[4]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[5]);
    TEST_ASSERT_EQUAL('5', test_Serial_Write_buff[6]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[7]);
    TEST_ASSERT_EQUAL('A', test_Serial_Write_buff[8]);
    TEST_ASSERT_EQUAL('4', test_Serial_Write_buff[9]);
    TEST_ASSERT_EQUAL('\r', test_Serial_Write_buff[10]);

}

void test_ChkCom (void)
{
    UnitySetTestFile (__FILE__);
    RUN_TEST(test_Overflow_OnCanTx);
    RUN_TEST(test_NormalRxStdFrame);
    RUN_TEST(test_NormalRxStdRTRFrame);
    RUN_TEST(test_NormalRxExtFrame);
    RUN_TEST(test_NormalRxExtRTRFrame);
    RUN_TEST(test_OverflowOnCANRx);
}
