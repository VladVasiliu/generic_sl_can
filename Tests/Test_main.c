/*
 * Test_main.c
 *
 *  Created on: Dec 27, 2022
 *      Author: vlad
 */
#include "unity.h"

extern int test_ChkCmds (void);
extern int test_ChkCom (void);

int main (void)
{
    UNITY_BEGIN();
    test_ChkCmds();
    test_ChkCom();
    return (UNITY_END());
}
