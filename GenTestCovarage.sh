lcov --directory ./Debug --rc lcov_branch_coverage=1 --capture --output-file app.info &&
lcov --remove app.info '*Unity*' '*Tests*'  --rc lcov_branch_coverage=1 --output-file app.info &&
genhtml --rc lcov_branch_coverage=1 app.info -o ./Cov