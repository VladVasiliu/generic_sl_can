#ifndef _TYPES_
#define _TYPES_

#ifndef NULL
#define NULL ((void *)0)
#endif

#include <stdint.h>
typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;

typedef int8_t  int8;
typedef int16_t int16;
typedef int32_t int32;



#endif
