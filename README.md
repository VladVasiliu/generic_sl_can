# generic_sl_can

This is a generic implementation of the well known ASCII sl can protocol.
 

## Getting started

Use the content of src folder inside of your project.

There are 2 interfaces that must be cofigured :

	1. Serial Communication
	    The following functions must be provided :
		```
		uint8 (*isDataAvailable)(void);
		uint8 (*GetByte)(void);
		uint8 (*isTxReady)(void);
		void  (*SetByte)(uint8 data);
		```

	2. Can Communication
		The following functions must be provided :
		```
		uint8 (*CanStart)(void);
		void  (*CanStop)(void);
		uint8 (*SetBaudrate)(uint32 Baud);
		uint8 (*SetBtr)(uint8 BTR0,uint8 BTR1);
		uint8 (*CanTx)(uint32 ID, uint8 dlc, uint8 *data);
		uint8 (*CanSetCodeRegister)(uint32 code);
		uint8 (*CanSetMaskCodeRegister)(uint32 mask);
		uint8 (*CanReadStatusFlags)(void);
		```
		Function : slcan_OnCanReceive must be called  when a CAN frame was received . 
   
## License 
This Code is licensed under MIT License.

## Project status
 Not Completed 
